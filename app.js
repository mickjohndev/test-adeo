const utils = require('./utils/utils');
const DATA = require('./data');

let data = DATA.data;

// get arguments
const args = process.argv.slice(2);
console.log("args", args);

// process data with arguments provided
data = utils.manageArgs(data, args);

console.log("Data processed", JSON.stringify(data, null, 2));
