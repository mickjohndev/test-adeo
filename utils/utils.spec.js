const utils = require('./utils');
const DATA = require('../data').data;

describe('utils', () => {
    describe('filterData', () => {
        test('should not alter data if search value is null', () => {
            // Arrange
            const expectedData = DATA;
            const searchValue = null;

            // Act
            const data = utils.filterData(DATA, searchValue);

            // Assert
            expect(data).toEqual(expectedData);
        });

        test('should not alter data if search value is empty', () => {
            // Arrange
            const expectedData = DATA;
            const searchValue = '';

            // Act
            const data = utils.filterData(DATA, searchValue);

            // Assert
            expect(data).toEqual(expectedData);
        });

        test('should filter correctly', () => {
            // Arrange
            const expectedData = [{
                name: 'Uzuzozne',
                people:
                    [
                        {
                            name: 'Harold Patton',
                            animals:[
                                {name: 'Raccoon dog'}
                            ]
                        }
                    ]
            }];

            const searchValue = 'Raccoon dog';

            // Act
            const data = utils.filterData(DATA, searchValue);

            // Assert
            expect(data).toEqual(expectedData);
        });

        test('should not be sensible to case', () => {
            // Arrange
            const expectedData = [{
                name: 'Uzuzozne',
                people:
                    [
                        {
                            name: 'Harold Patton',
                            animals:[
                                {name: 'Raccoon dog'}
                            ]
                        }
                    ]
            }];

            const searchValue = 'raccoon dOg';

            // Act
            const data = utils.filterData(DATA, searchValue);

            // Assert
            expect(data).toEqual(expectedData);
        });
    });

    describe('addChildrenCountToParentName', () => {
        test('should set correctly countries and peoples names', () => {
            // Arrange
            const expectedData = [{
                name: 'Uzuzozne [1]',
                people:
                    [
                        {
                            name: 'Harold Patton [1]',
                            animals:[
                                {name: 'Raccoon dog'}
                            ]
                        }
                    ]
            }];

            const dataToTransform = [{
                name: 'Uzuzozne',
                people:
                    [
                        {
                            name: 'Harold Patton',
                            animals:[
                                {name: 'Raccoon dog'}
                            ]
                        }
                    ]
            }];

            // Act
            const data = utils.addChildrenCountToParentName(dataToTransform);

            // Assert
            expect(data).toEqual(expectedData);
        });
    });

    describe('manageArgs', () => {
        test('should return data as it when argument is not managed', () => {
            // Arrange
            const args = ['dumb'];

            // Act
            const data = utils.manageArgs(DATA, args);

            // Assert
            expect(data).toEqual(DATA);
        });


        test('should filter data when filter argument is defined', () => {
            // Arrange
            const args = ["-f=Raccoon"];

            const expectedData = [{
                name: 'Uzuzozne',
                people:
                    [
                        {
                            name: 'Harold Patton',
                            animals:[
                                {name: 'Raccoon dog'}
                            ]
                        }
                    ]
            }];

            // Act
            const data = utils.manageArgs(DATA, args);

            // Assert
            expect(data).toEqual(expectedData);
        });

        test('should set name when count argument is defined', () => {
            // Arrange
            const args = ["-c"];

            const rawData = [{
                name: 'Uzuzozne',
                people:
                    [
                        {
                            name: 'Harold Patton',
                            animals:[
                                {name: 'Raccoon dog'}
                            ]
                        }
                    ]
            }];

            const expectedData = [{
                name: 'Uzuzozne [1]',
                people:
                    [
                        {
                            name: 'Harold Patton [1]',
                            animals:[
                                {name: 'Raccoon dog'}
                            ]
                        }
                    ]
            }];

            // Act
            const data = utils.manageArgs(rawData, args);

            // Assert
            expect(data).toEqual(expectedData);
        });

        test('should filter and set data name when filter and arguments are defined', () => {
            // Arrange
            const args = ["-f=Raccoon", '-c'];

            const expectedData = [{
                name: 'Uzuzozne [1]',
                people:
                    [
                        {
                            name: 'Harold Patton [1]',
                            animals:[
                                {name: 'Raccoon dog'}
                            ]
                        }
                    ]
            }];

            // Act
            const data = utils.manageArgs(DATA, args);

            // Assert
            expect(data).toEqual(expectedData);
        });
    })
});