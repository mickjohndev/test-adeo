const filterData = (data, filterText) => {
    // make sure that null and undefined return empty string
    filterText = filterText ? filterText : '';

    data = data.filter(item => {
        const foundPeople = item.people.filter(p => {

            const foundAnimals = p.animals.filter(a => {
                    const find = a.name.toLowerCase().indexOf(filterText.toLowerCase()) > -1;
                    return find;
                }
            );

            // new animals for people
            p.animals = foundAnimals;

            return foundAnimals.length > 0 ? true : false;
        });
           
        // new people for country
        item.people = foundPeople;

        return foundPeople.length > 0 ? true : false;
    });

    return data;
};

const addChildrenCountToParentName = (data) => {
    data = data.map(
        item => {
            // add people count to country
            item.name = item.name + ' [' + item.people.length + ']';

            item.people.map(
                p => {
                    // add animals count to people
                    p.name = p.name + ' [' + p.animals.length + ']';
                }
            )

            return item;
        }
    );

    return data;
};

const manageArgs  = (data, args) => {
    for (let i = 0; i < args.length; i++) {
        const arg = args[i];
        const keyValue = arg.split('=');

        // retrieve key and value
        const key = keyValue[0];
        const value = keyValue[1];

        // could use 2 ifs, but a switch is easily extensible if needed
        switch (key) {
            case '--filter':
            case '-f':
                data = filterData(data, value);
                break;
            case '--count':
            case '-c':
                data = addChildrenCountToParentName(data);
                break;
            default:
                console.log('No args, nothing changed.');
        }

    }
    
    return data;
}

module.exports.filterData = filterData;
module.exports.addChildrenCountToParentName = addChildrenCountToParentName;
module.exports.manageArgs = manageArgs;